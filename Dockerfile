FROM node:lts-alpine3.18 AS base
RUN apk add --no-cache libc6-compat bash python3 g++
WORKDIR /app
COPY app/package*.json ./

FROM base AS prod-deps
RUN npm ci --no-audit --omit dev

FROM base AS build-deps
RUN npm ci --no-audit

FROM build-deps AS build
COPY app ./
RUN npm run build

FROM base AS runtime
COPY --from=prod-deps /app/node_modules ./node_modules
COPY --from=build /app/dist ./dist

ENV HOST=0.0.0.0
ENV PORT=4321
EXPOSE 4321
CMD node ./dist/server/entry.mjs
