# Personal Website and Homepage of Full-Stack Developer Daniel Stopp


Seting up this project is easy, just clone the repo:


```bash
git clone https://gitlab.com/stoppdev-homepage/homepage.git homepage
cd homepage
```


Install the dependencies:

```bash
# npm
npm install

# pnpm
pnpm install

# Or use whatever your favourite package manager is.
# You know the drift, so I'll keep using npm as an example.
```


Then run the project locally:

```bash
npm run dev
```


To build and preview it:
```bash
npm run build && npm run preview
```

# YML files
These are used for CI/CD by me, you probably don't need them.


# What now?
You can do with the code whatever you like, as long as you adhere to the MIT-license and refrain from impersonating me :)
