import { defineConfig } from "astro/config";
// import markdoc from "@astrojs/markdoc";
import mdx from "@astrojs/mdx";
import node from "@astrojs/node";
import sitemap from "@astrojs/sitemap";

// https://astro.build/config
export default defineConfig({
  site: "https://daniel.stopp.dev",
  base: ".",
  outDir: "./dist",
  publicDir: "./static",
  compressHTML: true,
  prefetch: true,
  output: "hybrid",
  adapter: node({
    mode: "standalone",
  }),
  devToolbar: {
    enabled: false,
  },
  i18n: {
    defaultLocale: "en",
    locales: ["en"],
  },
  integrations: [
    mdx(),
    sitemap({
      i18n: {
        defaultLocale: "en",
        locales: {
          en: "en-US",
        },
      },
    }),
  ],
  markdown: {
    // https://github.com/shikijs
    shikiConfig: {
      theme: "nord",
      langs: [
        "astro",
        "rust",
        "elixir",
        "erlang",
        "go",
        "python",
        "ts",
        "css",
        "html",
        "jsx",
        "svelte",
        "hcl",
        "yml",
        "haskell",
      ],
      wrap: true,
    },
  },
});
