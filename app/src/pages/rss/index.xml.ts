import type { APIRoute } from "astro";

import rss, { type RSSFeedItem } from "@astrojs/rss";
import { getCollection } from "astro:content";
import sanitizeHtml from "sanitize-html";
import MarkdownIt from "markdown-it";
import { BLOG_PATH } from "@consts";

const parser = new MarkdownIt();
const blogPath = BLOG_PATH.split("/")[1];

export const GET: APIRoute = async ({ site }: { site: URL | undefined }) => {
  const siteURL: string = site?.href || "";
  const posts = await getCollection("posts", ({ data }) => !data.draft && data.pubDate <= new Date());
  const sortedPosts = [...posts].sort((a, b) => Number(new Date(b.data.pubDate)) - Number(new Date(a.data.pubDate)));
  const items = sortedPosts.map(
    (post): RSSFeedItem => ({
      title: post.data.title,
      categories: post.data.categories,
      description: post.data.summary,
      pubDate: post.data.pubDate,
      link: `${site}${blogPath}/${post.slug}`,
      customData: `<atom:link href="${site}${blogPath}/${post.slug}/" rel="self" type="application/rss+xml" />`,
      content: sanitizeHtml(parser.render(post.body)),
      author: "Daniel Stopp<daniel@stopp.dev>",
    })
  );

  return rss({
    xmlns: {
      atom: "http://www.w3.org/2005/Atom",
      content: "http://purl.org/rss/1.0/modules/content/",
    },
    site: siteURL,
    title: "daniel.stopp.dev",
    description: "Personal homepage and blog of web developer Daniel Stopp",
    customData: [
      `<language>en-us</language>`,
      `<atom:link href="${site}${blogPath}/rss.xml" rel="self" type="application/rss+xml" />`,
    ].join(""),
    stylesheet: "/styles/rss.xsl",
    items,
    trailingSlash: true,
  });
};
