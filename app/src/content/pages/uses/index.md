---
title: "Uses"
slug: "uses"
summary: "I enjoy uses pages which is a place on your homepage where you tell your readers what kind of software and hardware you're using. Of course I had to create such a page as well."
---

### Home Office Workspace

- **Desk:** IKEA Bekant
- **Headphones:** Bose QuietComfort 35 II
- **Monitor:** BenQ EL2870U 4K Monitor
- **Webcam:** BENEWY C1 (1920x1080)
- **Keyboard:** Royal Kludge RK918 RGB Mechanical Gaming Keyboard
- **Mouse:** Logitech MX Anywhere 2S Wireless Mouse

### Desktop PC

- **CPU:** AMD Ryzen 7 5800X
- **Memory:** 32GB (4x 8GB) G.Skill Flare X (DDR4-3200)
- **Graphics:** 8GB Asus GeForce GTX 1070 Dual OC
- **Hard drives:** 500GB Crucial P1 NVMe M.2 2280, 256GB Kingston SDD, 128GB Samsung SDD

### Laptop

- **Model:** Lenovo IdeaPad 5 Pro
- **Display:** 16 Inch WQXGA WideView/Anti-Glare
- **CPU:** AMD Ryzen 9 5900HX
- **Memory:** 32GB RAM
- **Hard drive:** 1TB SSD
- **Graphics:** NVIDIA GeForce RTX 3050 4GB GDDR6

### Software

- **OS:** Linux Mint (Desktop); Arch Linux (Laptop)
- **Window Manager:** i3
- **Window Tiling:** TMUX
- **Code Editor:** Neovim
- **Shell:** Zsh
- **Sexy Command Line Tools**: lf, fzf, exa, imagemagick, bat, ripgrep
- **Browsing:** Hardened Firefox
- **Password manager:** BitWarden

### Languages and Frameworks

- **Basics:** HTML, CSS, XML, SVG, XSLT
- **JavaScript/TypeScript:** Node.js, Express, HTMX, AlpineJS, Astro, SvelteKit, React, Next.js, Vue.js, SolidJS, Bun, Vite, Webpack
- **Go:** Templ, pgx, SQLx, GORM
- **Elixir/Erlang:** Phoenix Framework, Ecto, Hex
- **Python:** FastAPI, SQLAlchemy, Poetry, Alembic
- **APIs:** REST, GraphQL, JSON, gRPC, XML
- **Databases:** PostgreSQL, MySQL, MariaDB, CouchDB, MongoDB, Redis
- **Rust:**: Axum, Rocket, Cargo, Diesel

### DevOps and Source Control

- **Source Control:** Git, GitLab, GitHub
- **VPS/Hosting:** DigitalOcean, Netlify, Render, Fly.io
- **Containerization:** Docker, Docker Compose, Portainer
- **Servers:** Let's Encrypt, Traefik, NGINX, Apache

### Designing and Prototyping

- **Vim** and **Markdown**
- **Figma**
- **drawSQL**
- **Mermaid**
