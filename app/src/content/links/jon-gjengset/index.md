---
name: "Jon Gjengset"
draft: false
pubDate: 2023-10-05T11:11:11+02:00
summary: "Jon Gjengset has a YouTube channel on which he, among other things, dissects parts of the Rust standard library and popular Rust crates like Axum and Serde. If you're learning Rust, this his videos are fantastic resources for learning and understaning Rust."
projects:
  - title: "YouTube Channel"
    href: "https://www.youtube.com/@jonhoo/videos"
topics:
  - Rust
  - Code Walkthroughs
---

