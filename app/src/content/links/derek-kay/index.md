---
name: "Derek Kay"
draft: false
pubDate: 2023-09-09T11:11:11+02:00
summary: "A front-end developer, speaker and accessibility advocate who ispired me to polish up my RSS feed page."
projects:
  - title: "Personal Blog"
    href: "https://darekkay.com/"
    rss: "https://darekkay.com/atom.xml"
topics:
  - Development
  - Accessibility
---
