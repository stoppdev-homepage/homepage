---
name: "Dave Rupert"
draft: false
pubDate: 2023-09-09T11:11:11+02:00
summary: "Writes about lots of stuff, but I particularly enjoy his smart and rather unorthodox writing style."
projects:
  - title : "Personal Blog"
    href: "https://daverupert.com/"
    rss: "https://daverupert.com/atom.xml"
topics:
  - Writing
  - Development
  - Life
---
