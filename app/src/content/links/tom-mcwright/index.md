---
name: "Tom McWright"
draft: false
pubDate: 2023-09-09T11:11:11+02:00
summary: "I gotta admit, I read his content mainly because of his interesting book reviews, but Tom McWrights blog content is also pretty good."
projects:
  - title: "Personal Homepage"
    href: "https://macwright.com/"
topics:
  - Book Reviews
  - Blogging
  - Photography
---

