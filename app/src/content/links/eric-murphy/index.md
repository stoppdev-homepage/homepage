---
name: "Eric Murphy"
draft: false
pubDate: 2023-09-09T11:11:11+02:00
summary: "Eric Murphy is a web developer and YouTuber whose content inspired me to create link and uses sections on my homepage as well. I highly recommend his YouTube videos about privacy, security and the classic web."
projects:
  - title: "Personal Blog"
    href: "https://ericmurphy.xyz/"
    rss: "https://ericmurphy.xyz/index.xml"
topics:
  - Privacy
  - Simplicity
  - Development
---

