---
name: "Jared White"
draft: false
pubDate: 2023-09-09T11:11:11+02:00
summary: "The Spicy Web is a blog that I enjoy reading, because its author Jared White writes about two things that have diverted quite a lot over the last few years: frontend and simplicity. His personal blog is also fun and engaging to read."
projects:
  - title: "Personal Blog"
    href: "https://jaredwhite.com/"
  - title: "Development Blog"
    href: "https://www.spicyweb.dev/"
topics:
  - Development
  - Photography
  - Life
---

