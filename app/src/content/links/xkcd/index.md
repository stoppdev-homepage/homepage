---
name: "xkcd (Randall Munroe)"
draft: false
pubDate: 2023-09-09T11:11:11+02:00
summary: "Creative, intelligent and fun comics more often than not with a philosophical edge. Not exactly a secret tip on the internet, but I like to recommend xkcd any chance."
projects:
  - title: "Webcomics"
    href: "https://xkcd.com/"
    rss: "https://xkcd.com/rss.xml"
topics:
  - Life
  - Philosophy
  - Language
---

