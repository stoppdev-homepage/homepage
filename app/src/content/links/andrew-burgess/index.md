---
name: "Andrew Burgess"
draft: false
pubDate: 2023-09-09T11:11:11+02:00
summary: "Very good web development articles on TypeScript, CSS, Rust, TMUX, algorithms and many topics I find interesting. I can also recommend his YouTube videos."
projects:
  - title : "Personal Blog"
    href: "https://shaky.sh/"
    rss: "https://shaky.sh/feed.json"
  - title : "Videos (YouTube)"
    href: "https://www.youtube.com/c/andrew8088"
topics:
  - TypeScript
  - Development
  - Tech-Blogging
---
