---
title: "How I realized intuitive design is far more valuable than practice"
summary: "Page generators are probably the best choice for web developers when it comes to blogging and building one-pagers, landing pages and the like. There are dozens of page generators and light markup- and style-centric frameworks to choose from. For the longest part of my active career, Hugo was my favourite page generator, but due to different reasons than I thought."
pubDate: 2023-12-01T00:00:00+02:00
draft: false
authors:
  - daniel
categories:
  - Design
tags:
  - Hugo
  - Technology
---

## An example in page generators

Page generators are probably the best choice for web developers when it comes to blogging and building one-pagers, landing pages and the like. There are dozens over dozens of page generators and light markup- and style-centric frameworks to choose from. For the longest part of my active career, Hugo was my favourite page generator, but not due to of the reasons I thought. When I realized that, I went on and searched for a better alternative.


## The usual disclaimer

I am not trying to belittle Hugo or make fun of it. In fact, similar comparisons could be made with other frameworks or technologies, I've used Hugo for several dozens of personal and client projects, and it's still one of the best tools for prototyping and creating small pages. However, it's a pretty old technology when comparing its existence to the age of the web. I probably had to set that straight, because many, many developers still use it and love it. That's quite allright, who am I to tell you what to build your websites with? I just want you to acknowledge why it simply doesn't work for me anymore and how the manner in which I realized it helped me understand a fact which, no matter how insignificant it may seem to you, wasn't obvious at all to me.


## I know how it works, so it must be easy, right?

I mean, create some layouts, styles and markup to present your future content, add shortcodes and extend some functionality, if you please, then set up archetypes to generate your future content, and all that's left is to write said content. A simple `git push` and some minor settings at your FrontendOps service of choice later, then lo and behold the result is online. Couldn't be easier, right?

But Think about it, are the technologies you use on a daily basis actually still as simple as you perceive them to be or do they only seem intuitive, because you're used to them? Have they always been easy to use or have they become more complicated over time, without you actually realizing it, because bad decisions were made and applied in small doeses rather than all at once?


## A sudden shift in perspective

My perspective, on Hugo in particular, shifted a lot after a hiatus of just over a year, because I was busy with alrger work projects, sort of burned out and thus only playing video games in my rare spare time instead of working on smaller projects I would formerly have used Hugo for. When I revisited a hobby project after that hiatus, however, Hugo sddenly seemed tedious, not to say annoiyng to use, since I simply wasn't used to its more unintuitive parts anymore and thus I was suddenly forced to think about things that shouldn't be hard to figure out.

Suddenly my mindset shifted from "let's do this real quickly" to "how the fuck do I have to do this again?" over to "why the fuck does this have to be so damn complicated?". For example, I had completely forgotten, how clunky Go templating was, how many weird gotchas it yields and how unintuitively structured the Hugo docs were. I actually had to search and check Stack Overflow a lot to find out how to achieve the simplest things, precisely because they actually neither were as simple as I previously perceived them to be nor as intuitive as I nowadays felt they should be.


## When a subconscious fact turns into an epiphany

When I was still used to it, I knew exactly how everything worked that I needed for my projects or where to find it in the docs that I din't really have to search for it anymore either, and so I wondered if the bad parts had gotten worse over the years or if I had previously just dealt with the weird and annoying parts, because there was no better alternative at that time.

Based upon the recent questions asked by new users which I came across while trying to get back into Hugo myself, the hurdle seems to have grown big enough that there are probably more Hugo users now who are just used to it than actual new users. At least the number of the latter seems to be dwindling.

Designing in a way that is as intuitive as possible when does not only get you further, but also helps your users learn and utilize your products more than relying on routine, practice and habit. Learning something and being productive with it is one thing, but being able to quickly grasp concepts intuitively is worth so much more, because it makes the switch to similar technologies easier which again elevates progress and makes you more productive.

The more used to something you are, because it you needed to learn it and learning it was a pain, the more you will be reluctant to change to something newer or even arguably better as long as the bad experience you made learning the current thing is still lingering in the back of your mind.
