import { defineCollection, reference, z } from "astro:content";

const authors = defineCollection({
  type: "content",
  schema: ({ image }) => z.object({
    name: z.string(),
    summary: z.string(),
    avatarAlt: z.string().optional(),
    avatarSrc: image().optional(),
  }),
});

const pages = defineCollection({
  type: "content",
  schema: ({ image }) => z.object({
    title: z.string(),
    summary: z.string().optional(),
    heroImageAlt: z.string().optional(),
    heroImageSrc: image().optional(),
  }),
});

const posts = defineCollection({
  type: "content",
  schema: ({ image }) => z.object({
    title: z.string(),
    summary: z.string(),
    pubDate: z.date(),
    draft: z.boolean(),
    authors: z.array(reference("authors")),
    categories: z.array(z.string()),
    tags: z.array(z.string()),
    heroImageAlt: z.string().optional(),
    heroImageSrc: image().optional(),
  }),
});

const reviews = defineCollection({
  type: "content",
  schema: ({ image }) => z.object({
    title: z.string(),
    summary: z.string(),
    pubDate: z.date(),
    draft: z.boolean(),
    author: z.string(),
    authors: z.array(reference("authors")),
    categories: z.array(z.string()),
    tags: z.array(z.string()),
    coverAlt: z.string().optional(),
    coverSrc: image().optional(),
    publisher: z.string(),
    bookPubDate: z.date(),
    medium: z.string(),
    pages: z.number(),
    language: z.string(),
  }),
});

const links = defineCollection({
  type: "content",
  schema: z.object({
    name: z.string(),
    draft: z.boolean(),
    pubDate: z.date(),
    summary: z.string(),
    projects: z.array(
      z.object({
        title: z.string(),
        href: z.string().url(),
        rss: (z.string().url()).optional().or(z.literal("")),
      })
    ),
    topics: z.array(z.string()),
  }),
});

export const collections = {
  authors,
  pages,
  posts,
  reviews,
  links,
};
