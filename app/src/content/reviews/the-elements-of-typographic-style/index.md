---
title: "The Elements of Typographic Style"
author: "Robert Bringhurst"
summary: "The most thorough work on typography and its uses I've ever come across"
pubDate: 2023-08-11T12:12:12+02:00
draft: false
authors:
  - daniel
categories:
  - typography
tags:
  - functionality
  - beauty
  - art
coverSrc: "./images/the-elements-of-typographic-style.jpg"
coverAlt: "The book cover of The Elements of Typographic Style"
publisher: "Hartley & Marks Publishers"
bookPubDate: 2013-01-05T00:00:00+02:00
medium: "Paperback"
language: "English"
pages: 352

---

I love typography. I'm not amazing at it, but I think it's a very interesting discipline, and believe it or not, but "The Elements of Typographic Style", thus Robert Bringhurst, were mainly responsible for me to actually get into web design, rather than just frontend development, because it got me obsessed with typography for the better part of a year. The subtleties of type, the simple rules to follow in order to make mere written words look absolutely stunning, even tranform them into something so visually pleasing that you'll just **want** to read it, and to make your reading more fluent, was absolutely fascinating to me.

I've read about this book so often before I actually got to read the book itself, that my expectations were extremely high (everybody just lauded the book), yet it succeeded my expectations. I remember when I was on my way to university by train once, but didn't even notice I had already reached the final stop, because I stumled so deep into this fantastic book that I was completely immersed. It's not a novel, mind you, but non-fiction.
