---
author: "Robert Nystrom"
title: "Crafting Interpreters"
summary: "Read this. Honestly, in the top three of the best tech-books I've evber read"
pubDate: 2023-08-11T12:12:12+02:00
draft: false
authors:
  - daniel
categories:
  - Programming
  - Rust
tags:
  - interpreters
coverSrc: "./images/crafting-interpreters.jpg"
coverAlt: "Cover of Crafting Interpreters by Robert Nystrom"
publisher: "Genever Benning"
bookPubDate: 2031-01-28T00:00:00+02:00
medium: "Paperback"
language: "English"
pages: 639
---

Since this isn't really a review, but more unconditional praise, I'll kill the supsense right away: if you want to learn how to write interpreters and parsers – particularly in C++ or Java, you can get a fantastic overview just by reading this book. I've read it and tried to port the C++ and Java code to Rust, which worked like a charm.

That being said, I'm currently developing a learning platform (more for fun and learning purposes than for any other gain) and this book actually tought me how to write a small Domain Specific Language (DSL) complete with an interpreter for it, which is not only pretty damn cool, but clearly a useful skill to have.

Yes, you can [read it online](https://craftinginterpreters.com/contents.html), but it's a fantastic write up, really well-composed and I really think you should do what I did and buy a physical copy of the book. I know what you might think now, but I don't earn any money or gain anything else from you buying the book, pinky swear. There is just so much effort in this book, even if you were the most egocentric person, just buy it, because it makes it more likely that Robert Nystrom will write more great books, m'kay?
