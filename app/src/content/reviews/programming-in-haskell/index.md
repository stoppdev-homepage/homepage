---
title: "Programming in Haskell"
author: "Graham Hutton"
summary: "Probably the best printed resource on programming Haskell next to \"Learn You a Haskell For Great Good\""
pubDate: 2023-08-11T12:12:12+02:00
draft: false
authors:
  - daniel
categories:
  - programming
tags:
  - pure
  - functional
coverSrc: "./images/programming-in-haskell.jpg"
coverAlt: "The book cover of Programming in Haskell"
publisher: "Cambridge University Press"
bookPubDate: 2016-09-16T00:00:00+02:00
medium: "Paperback"
language: "English"
pages: 320

---


Every once in a while, I just feel the urge to try something new, particularly when I have dealt with one and the same programming language for a longer period of time.

A few years ago I chose Haskell, even though I'm pretty much rooted in web development and knowing very well that Haskell is not quite suited for web developers. I was far more interested in functional programming anyway, rather than in the language per se. I wanted my new language to feel different than any other language I had used before, which were PHP (yuck! Sorry, but I really hate PHP), JavaScript, which I've always had a soft spot for, because it's so expressive and simple, whether it's a foot gun or not, and well, &hellip; that was all, up until then.


## Broaden your horizon – learn a new language

Haskell is just perfect for the purpose of broadening your horizon in programming, because it's not a widely used language (it's actually even rather academic, due to its proximity to mathematics, particularly lambda calculus, which I find fascinating in its own right – but, sorry, I diverge), but it's basically **the** functional programming language. The most interesting part is, how extremely weird it feels the first time you use it, at least if you're used to Object Oriented Programming or virtually any other paradigms than functional programming.


## Useless enough to be fun

With the language chosen along with the reason to learn it, it was interesting enough of an endeavour to buy "Learn You a Haskell for Great Good" (even though it's available for free, but I love books and I wanted to support the author), the title of which wasn't the only weird thing about that book. Don't get me wrong, it's a great book, written in a very funny way, enriched with drawings that hit the exact medium of strangely ingenious and completely deranged. However, to my mind it doesn't compare to "Programming in Haskell" at all when it comes to concise and useful examples of how amazing Haskell is as a language.


## Care for an example?

The reason, why I think that "Programming in Haskell" is probably the best book to read, if you're trying to learn Haskell, is, the way the author Graham Hutton introduces list comprehensions and composition at the same time by showing three simple functions and how to combine them to create a prime number filter:

```haskell
factors :: Int -> [Int]
factors n = [x| x <- [1..n], n 'mod' x == 0]

> factors 15
[1, 3, 5, 15]
```

Easy enough, let's use that function to create a prime function:

```haskell
prime :: Int -> Bool
prime n = factors m == [1, n]

> prime 15
false
> prime 7
true
```

And then use that again to filter primes between 2 and n:

```haskell
primes :: Int -> [Int]
primes n = [x | x <- [2..n], prime x]

> primes 40
[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]
```

As little performant as it may be, I found this to be an extremely elegant set of examples, and if that doesn't check a whole bunch of boxes at once, I don't know what does.

So, if you're anything like me and are trying to learn Haskell, I recommend you start by reading "Programming in Haskell" first, then have fun with "Learn You a Haskell for Great Good". Of course I do love fun, comedy and nonsense like the next guy, all of them are great vehicles for swift learning, but really only when well-dosed and utilized in a non-excessive way. In my opinion, though, perfectly chosen examples and dense material that is still easy and light to read, on the other hand, almost always wins as far as didactics are concerned.
