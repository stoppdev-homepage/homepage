---
name: "Daniel"
summary: "A full-stack web developer, heavy metal maniac and literature nerd, living in Berlin."
avatarSrc: "./images/profile.jpg"
avatarAlt: "Daniel Stopp"
---

This is my personal homepage and blog, where I write about development and work experiences, review interesting books and link to stuff on the web that I enjoy.

I like creating fast websites, traveling with my girl&shy;friend, func&shy;tio&shy;nal pro&shy;gram&shy;ming, learning new stuff, writing about pro&shy;gram&shy;ming, pro&shy;gramm&shy;ing languages and frame&shy;works, and listening to lots of music, mainly Heavy Metal and a lot of its subgenres. My current favourite tools to build websites are plain ol' HTML/CSS, Astro, SvelteKit, TypeScript and/or JavaScript and Golang. I also use Python a lot – even though I don't particulary like it, but sometimes its the best tool for the job. Occasionally I enjoy building stuff with Elixir or Rust or learning Haskell, in order to solve specific problems or build more complex websites. Lately, I'm very interested in DevOps and software architecture patterns as well. If you really want to learn more about what other useful tools I've put [on my belt](/uses#software) over the years, have a look at my [/uses](https://uses.tech/) page.

I do freelance contracting and consulting work, and I am always on the lookout for new job and learning opportunities, so if you think I'm the right guy for an interesting job you've got in the pipeline, drop me a line.
