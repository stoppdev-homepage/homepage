export interface Employer {
  name: string;
  href: string;
}

export interface Work {
  employer: Employer;
  title: string;
  tasks: string[];
  description: string;
  from: string;
  to: string | null;
}

export interface CV {
  education: string[];
  work: Work[];
  status: string;
}

export const data = {
  education: [
    "I have regular Abitur (vaguely equivalent to a High School Diploma) with very good grades, which I obtained via nightschool (Abendgymnasium Sophie Scholl, Osnabrück) from 2006 to 2009.",
    "I've studied History and German Studies until my passion for web development made me change my career in 2013.",
    "I don't feel the need to put my whole life story on the internet, just the important parts. So, if you want to know more about me than the basics, please don't hesitate to contact me :)",
  ],

  work: [
    {
      employer: {
        name: "50Hours",
        href: "https://www.morgenpost.de/berlin-aktuell/startups/neu-in-berlin/article123048841/50Hours-Kostenlose-App-fuer-Last-Minute-Tickets-in-Berlin.html",
      },
      title: "Werkstudent",
      tasks: ["Design", "Design Implementation", "Frontend Entwicklung", "Content Management"],
      description:
        "50Hours war ein leider recht kurzlebiges berliner Startup, das eine gleichnamige Mobil-App entwickelt hat. 50Hours sollte es Benutzern ermöglichen Last-Minute Tickets für eine breite Auswahl an Events zu buchen. Dies war das erste mal, dass ich regulär für die Arbeit bezahlt wurde (als Werkstudent), die ich zuvor nur als Hobby betrieben hatte. Ursprünglich eingestellt, um dem Chef-Designer auszuhelfen, wurde ich schnell zum Frontend Entwickler. Da ich bereits mit WordPress, HTML, CSS und JavaScript gearbeitet hatte, wurde mir bald die Verantwortung für die 50Hours Landing Page übertragen, die ich auch mitgestaltete. Hier änderte ich meine bisherigen Zukunftspläne, und beschloss, Entwickler zu werden.",
      from: "Mai 2013",
      to: "Dezember 2013",
    },

    {
      employer: {
        name: "Self employed",
        href: "/",
      },
      title: "Freiberuflicher Fullstack Entwickler",
      tasks: ["Web Entwicklung", "Design-Implementation", "DevOps", "Konzeption", "Beratung"],
      description:
        "Da ich bereits einige Jahre vor meinem ersten Job Frontend Entwicklung als Hobby betrieben hatte, fühlte es sich ziemlich natürlich für mich an, die Arbeit weiterzuführen und mich freiberuflich zu betätigen. Ich blieb zunächst bei WordPress aber bewegte mich allmählich davon weg, um meinen Horizont zu erweitern. Ich betreibe die Arbeit immer noch freiberuflich, wenn sich Gelegenheiten ergeben, neige aber mittlerweile eher zu Festanstellung in Teilzeit Jobs, die mir das Freelancen nebenbei ermöglichen, weil ich die Abwechslung und die Lernmöglichkeiten genieße.",
      from: "Januar 2014",
      to: null,
    },

    {
      employer: {
        name: "Datalyze Solutions",
        href: "https://datalyze-solutions.com",
      },
      title: "Frontend/Backend Entwickler",
      tasks: ["Web Entwicklung", "Konzeption", "Design Implementation", "Kundenkontakt"],
      description:
        "Bei Datalayze Solutions weitete sich der Fokus meiner Arbeit zum ersten mal von kleinen Projekten und Landing Pages zu mittelgroßen bis großen Projekten mit separater API, realisiert mit Python und Flask oder Elixir und dem Phoenix Framework sowie aufwändigen JavaScript-Frontends in React. Projekte von Großkunden wie CBRE und Bosch ebneten mir den Weg künftig auch im Backend anstatt wie zuvor fast ausschließlich im Frontend zu arbeiten, sodass ich bald von ausschließlich freiberuflicher Arbeit zur Festanstellung bei Datalayze Solutuions wechselte.",
      from: "Juli 2017",
      to: "Juni 2021",
    },

    {
      employer: {
        name: "aedvice",
        href: "https://www.aedvice.de",
      },
      title: "Leitender Entwickler",
      tasks: ["Fullstack Entwicklung", "DevOps", "Consulting", "Software Architektur"],
      description:
        "aedvice (öffentlich bestellte Vermessung) war mein erster Arbeitgeber, der eigentlich nichts mit Web Entwicklung zu tun hat. Pläne von aedvice für die Entwicklung des Geoinformationssystems assetGIS änderte sich dies jedoch und die Möglichkeit, mit Freunden und ehemaligen Kollegen zu arbeiten, überzeugte mich. Mir bot bot sich die Möglichkeit, gleichzeitig meine Fähigkeiten zu nutzen, aber dabei viel Neues zu lernen, das mich zuvor schon eine Weile lang stark interessiert hatte. Unerfahrene und ungelernte Entwickler an meinen Erfahrungen teilhaben zu lassen, bereitete mir eben so viel Freude wie meine Kenntnisse von GitLab, CI/CD, Docker, Virtual Private Servern (kurz: DevOps) stark erweitern zu können.",
      from: "Juli 2021",
      to: "Dezember 2022",
    },

    {
      employer: {
        name: "repartners",
        href: "https://repartners.de",
      },
      title: "Leitender Entwickler",
      tasks: ["Fullstack Entwicklung", "DevOps", "Consulting", "Software Architektur"],
      description:
        "Da repartners von der Initiatorin des assetGIS-Projekts hervorging, an dem ich bereits bei aedvice gearbeitet hatte, änderte sich eigentlich nur der Name meines Arbeitgebers, das Projekt und meine Arbeit daran blieben allerdings die selben.",
      from: "Januar 2023",
      to: "März 2024",
    },
  ],

  status:
    "I am currently up for new tasks and experiences, where it's possible to utilize what I've learned so far and where I have the opportunity to learn new technologies, frameworks or programming languages. Learning is essential, even more so as a web developer and never ceasing to learn makes this field of work so exceptionally enjoyable to me, so don't hesitate to contact me, if you're interested in working with me.",
};
