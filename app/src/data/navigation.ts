export type LinkItem = { href: string, title: string, matches: string[] };
export type LinkItemList = Array<LinkItem>;

export const header: LinkItemList = [
  { href: "/", title: "About", matches: [], },
  { href: "/cv", title: "CV", matches: [], },
  { href: "/blog", title: "Blog", matches: [], },
  { href: "/reviews", title: "Reviews", matches: [], },
  { href: "/links", title: "Links", matches: [], },
  { href: "/uses", title: "Uses", matches: [], },
];

export const footer: LinkItemList = [
  { href: "/impressum",  title: "Impressum", matches: [], },
  { href: "/datenschutz", title: "Datenschutz", matches: [], },
  { href: "/rss.xml", title: "RSS", matches: [], },
];
