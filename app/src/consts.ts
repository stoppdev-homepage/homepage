// Main page paths
export const BLOG_PATH: string = "/blog";
export const REVIEWS_PATH: string = "/reviews";

// Sub page paths
export const BLOG_CATEGORIES_PATH: string = `${BLOG_PATH}/categories`;

// Page size
export const PAGE_SIZE: number = 12;

// Repo
export const REPO_URL: string = "https://gitlab.com/stoppdev-homepage"

// Authentication
export const AUTHED_ROUTES = ["/links"];
